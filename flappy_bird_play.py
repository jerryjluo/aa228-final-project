"""
Plays flappy bird using the trained DQN agent.
"""

import flappy_bird

game = flappy_bird.GameLoop(
    image_width=256,
    image_height=256,
    pipe_gap=100,
    image_shrink_factor=8,
    buffer_size=100000,
    epsilon=0,
    rpc_timeout=0.1,
    train_steps_per_iteration=100,
    iterations_per_train=100,
    serving_update_iterations=1000,
    min_buffer_size_for_train=64,
    display_screen=False,
    pause=0,
    learning_rate=1e-4,
    train_batch_size=32,
    gamma=0.95,
    tmp_model_dir='/tmp/flappy_model',
    exported_model_dir='/tmp/flappy_model_exported'
)

while True:
    game.play_episode()
